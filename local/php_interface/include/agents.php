<?

const MAIL_TEMPLATE = 33;
const GROUP_ADMIN = 1;

function ExamCheckCount() {
	$rsUsers = CUser::GetList([], [], ['ACTIVE' => 'Y'], ['ID']);
	$count = 0;
	while($arUser = $rsUsers->Fetch()) {
		$count++;
	}
	$rsUsers = CUser::GetList([], [], ['ACTIVE' => 'Y', 'GROUPS_ID' => [GROUP_ADMIN]], ['ID', 'EMAIL']);
	$arFields = ['COUNT' => $count];
	while($arUser = $rsUsers->Fetch()) {
		$arFields['EMAIL'] = $arUser['EMAIL'];
		if($arFields['EMAIL']) {
			CEvent::Send('REGISTRATION_COUNT', 's1', $arFields, 'N', MAIL_TEMPLATE);
		}
	}
	return "ExamCheckCount();";
}