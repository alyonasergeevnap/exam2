<?
const NEWS_IBLOCK = 1;

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("NewsIblockClass", "OnBeforeIBlockElementUpdateHandler"));

class NewsIblockClass
{
	// создаем обработчик события "OnBeforeIBlockElementUpdate"
	public static function OnBeforeIBlockElementUpdateHandler(&$arFields)
	{
		if($arFields["IBLOCK_ID"] == NEWS_IBLOCK) {
			$wordToCheck = "калейдоскоп";
			$wordToReplace = "[...]";
			if(strpos($arFields['PREVIEW_TEXT'], $wordToCheck) !== false) {
				$arFields['PREVIEW_TEXT'] = str_replace($wordToCheck, $wordToReplace, $arFields['PREVIEW_TEXT']);
				CEventLog::Add(array(
					"SEVERITY" => "INFO",
					"AUDIT_TYPE_ID" => "CHANGE_IBLOCK_ELEMENT",
					"MODULE_ID" => "main",
					"ITEM_ID" => $arFields['ID'],
					"DESCRIPTION" => "Замена слова калейдоскоп на [...], в новости с ID = " . $arFields['ID'],
				));
			}	
		}
	}
}

AddEventHandler('search', 'BeforeIndex', array('SearchHandler', 'BeforeIndexHandler'));

class SearchHandler 
{
	public static function BeforeIndexHandler($arFields)
	{
		if($arFields['MODULE_ID'] == 'iblock') {
			$arFields['TITLE'] = TruncateText($arFields['BODY'], 50);
		}
		return $arFields;
	}
}

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");

function OnBeforeIBlockElementAddHandler(&$arFields)
{
        // ех2-75
    if($arFields["IBLOCK_ID"] == NEWS_IBLOCK)
    {
    	if(strpos($arFields["PREVIEW_TEXT"], "калейдоскоп" ) !== false)
        {   
            global $APPLICATION;
            $APPLICATION->throwException("Мы не используем слово калейдоскоп в анонсах новостей!");
            return false;
        }
    }
}

AddEventHandler("main", "OnEpilog", "CheckNonExistPagesHandler");

function CheckNonExistPagesHandler()
	{
		if (defined("ERROR_404") && ERROR_404 == "Y")
		{
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
			include $_SERVER["DOCUMENT_ROOT"] . "/" . SITE_TEMPLATE_PATH . "/header.php";
			include $_SERVER["DOCUMENT_ROOT"] . "/404.php";
			include $_SERVER["DOCUMENT_ROOT"] . "/" . SITE_TEMPLATE_PATH . "/footer.php";
			CEventLog::Add(
				array(
					"SEVERITY"	=>	"INFO",
					"AUDIT_TYPE_ID"	=>	"ERROR_404",
					"MODULE_ID"	=>	"main",
					"DESCRIPTION"	=>	$APPLICATION->GetCurPage()
				)
            );
		}
	}
