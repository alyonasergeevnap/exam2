<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(isset($arResult['TITLE_NAME'])) {
	$APPLICATION->SetPageProperty('slogan_head', $arResult['TITLE_NAME']);
}
if(isset($arResult['DETAIL_PICTURE']) && $arResult['DETAIL_PICTURE']) {
	$APPLICATION->SetPageProperty('head_style', "background-image: url(" . $arResult['DETAIL_PICTURE']['SRC'] . "); background-size: contain;");
}